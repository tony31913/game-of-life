


// **** For the buttons
$("#clear-game").click(() => {
    stop()
    init()
    redraw()
})

$("#play-stop-button").click(() => {
    if (playing) {
        stop()
    } else {
        play()
    }
})

$("#random-button").click(() => {
    stop()
    init("random")
    redraw()

})

$("#draw-erase-button").click(() => {
    if (!erase) {
        erase = true
        $("#draw-erase-button").html('Draw<i class="fas fa-pencil-alt"></i>')
    } else {
        erase = false
        $("#draw-erase-button").html('Erase<i class="fas fa-eraser"></i>')
    }
})

$("#random-glider-button").click(() => {
    stop()
    randomGlider();
})

$("#diy-glider-button").click(() => {
    //shouldShowDiyGlider 
    glinderSignal = true
    teckyLogoSignal = false
})

$("#random-tecky-logo-button").click(() => {
    stop()
    //Wait the program to finish the last drawing
    setTimeout(randomTeckyLogo, 50)
})

$("#diy-tecky-logo-button").click(() => {
    teckyLogoSignal = true
    glinderSignal = false
})

$("#reset-rules-button").click(() => {
    $("#over-pop-input").val(3)
    overPopulationRule = 3
    $("#under-pop-input").val(2)
    underPopulationRule = 2
    $("#repro-input").val(3)
    reproductionRule = 3
})


// **** For the inputs
$("#fps-input").change(() => {
    let fPS = round($("#fps-input").val())
    if (fPS <= 0) {
        showModal("Frame rate should be at least 1.<br>If you need to stop the game, please press the stop button instead.")
        fPS = 1
        $("#fps-input").val(1)
    }
    frameRate(fPS)
})

$("#over-pop-input").change(() => {
    if (isNaN(parseInt($("#over-pop-input").val())) || round($("#over-pop-input").val()) <= 0) {
        showModal("Please input a valid number.")
        $("#over-pop-input").val(overPopulationRule)
    } else {
        overPopulationRule = round($("#over-pop-input").val())
    }
})

$("#under-pop-input").change(() => {
    const underPopCount = parseInt($("#under-pop-input").val());
    if (isNaN(underPopCount) || underPopCount <= 0) {
        showModal("Please input a valid number.")
        $("#under-pop-input").val(underPopulationRule)
    } else {
        underPopulationRule = round($("#under-pop-input").val())
    }
})

$("#repro-input").change(() => {
    if (isNaN(parseInt($("#repro-input").val())) || round($("#repro-input").val()) <= 0) {
        showModal("Please input a valid number.")
        $("#repro-input").val(reproductionRule)
    } else {
        reproductionRule = round($("#repro-input").val())
    }
})

$("#columns-input").change(() => {
    let maxColumns = floor(width / unitLength)

    if (isNaN(parseInt($("#columns-input").val())) || round($("#columns-input").val()) <= 0) {
        showModal("Please input a valid number.")
        $("#columns-input").val(columns)
    } else if (round($("#columns-input").val()) > maxColumns) {
        showModal("Your screen is not big enough to show the number of columns/ rows you've specified.<br>Please input a smaller value.")
        $("#columns-input").val(columns)
    } else {
        columns = round($("#columns-input").val())
        stop()
        init()
        redraw()
    }
})

$("#rows-input").change(() => {
    let maxRows = floor(height / unitLength)

    if (isNaN(parseInt($("#rows-input").val())) || round($("#rows-input").val()) <= 0) {
        showModal("Please input a valid number.")
        $("#rows-input").val(rows)
    } else if (round($("#rows-input").val()) > maxRows) {
        showModal("Your screen is not big enough to show the number of columns/ rows you've specified.<br>Please input a smaller value.")
        $("#rows-input").val(rows)
    } else {
        rows = round($("#rows-input").val())
        stop()
        init()
        redraw()
    }
})

// **** To control play or stop
function play() {
    loop()
    playing = true
    $("#play-stop-button").html('Stop<i class="fas fa-stop"></i>')
}

function stop() {
    noLoop()
    playing = false
    $("#play-stop-button").html('Play<i class="fas fa-play"></i>')
}

function showModal(msg) {
    $("#my-modal p").html(`${msg}`)
    $("#my-modal").modal("show")
}